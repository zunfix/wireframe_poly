use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::keyboard::Scancode;

extern crate ene;
// mod engine;
use ene::engine::entity::{aabb_collision, epa, Entity};
use ene::engine::polygon::Polygon;
use ene::engine::polygon::Transform;
use ene::engine::renderer::Renderer;
use ene::engine::vec2::Vec2;

const WIDTH: u32 = 1020;
const HEIGHT: u32 = 768;

struct Player {
    transform: Transform,
    points: Polygon,
    color: sdl2::pixels::Color,
    speed: f32,
}

impl Player {
    fn update(&mut self, event_pump: &sdl2::EventPump) {
        if event_pump
            .keyboard_state()
            .is_scancode_pressed(Scancode::Left)
        {
            self.transform.rotate -= 0.05;
        }

        if event_pump
            .keyboard_state()
            .is_scancode_pressed(Scancode::Right)
        {
            self.transform.rotate += 0.05;
        }

        if event_pump
            .keyboard_state()
            .is_scancode_pressed(Scancode::Up)
        {
            self.transform.translate.x += self.speed * self.transform.rotate.sin();
            self.transform.translate.y -= self.speed * self.transform.rotate.cos();
        }

        if event_pump
            .keyboard_state()
            .is_scancode_pressed(Scancode::Down)
        {
            self.transform.translate.x -= self.speed * self.transform.rotate.sin();
            self.transform.translate.y += self.speed * self.transform.rotate.cos();
        }

        if self.transform.translate.y > HEIGHT as f32 {
            self.transform.translate.y = 0.0;
        }
        if self.transform.translate.y < 0.0 {
            self.transform.translate.y = HEIGHT as f32;
        }
        if self.transform.translate.x > WIDTH as f32 {
            self.transform.translate.x = 0.0;
        }
        if self.transform.translate.x < 0.0 {
            self.transform.translate.x = WIDTH as f32;
        }
    }
}

impl Entity for Player {
    fn polygon(&self) -> Polygon {
        self.points.transform(&self.transform)
    }

    fn frame_update(&mut self, event_pump: Option<&sdl2::EventPump>) {
        if let Some(event) = event_pump {
            self.update(event)
        }
    }

    fn farthest_point(&self, direction: &Vec2) -> Vec2 {
        self.points
            .transform(&self.transform)
            .farthest_point(&direction)
    }
}

struct Object {
    transform: Transform,
    pub points: Vec<Vec2>,
    pub color: sdl2::pixels::Color,
}

impl Entity for Object {
    fn polygon(&self) -> Polygon {
        let points = self.points.to_vec();
        (Polygon { points }).transform(&self.transform)
    }

    fn frame_update(&mut self, event_pump: Option<&sdl2::EventPump>) {
        if let Some(_event) = event_pump {}
    }

    fn farthest_point(&self, direction: &Vec2) -> Vec2 {
        *self
            .polygon()
            .points
            .iter()
            .max_by(|a, b| a.dot(direction).partial_cmp(&b.dot(direction)).unwrap())
            .unwrap()
    }
}

fn gen_bounding_box(object: &dyn Entity) -> Polygon {
    let p1 = Vec2 {
        x: object.polygon().x_shadow().p1.x,
        y: object.polygon().y_shadow().p1.y,
    };

    let p2 = Vec2 {
        x: object.polygon().x_shadow().p2.x,
        y: object.polygon().y_shadow().p1.y,
    };

    let p3 = Vec2 {
        x: object.polygon().x_shadow().p2.x,
        y: object.polygon().y_shadow().p2.y,
    };

    let p4 = Vec2 {
        x: object.polygon().x_shadow().p1.x,
        y: object.polygon().y_shadow().p2.y,
    };

    Polygon {
        points: vec![p1, p2, p3, p4],
    }
}

fn main() -> Result<(), String> {
    let sdl_context = sdl2::init()?;
    let mut renderer = Renderer::new(&sdl_context, "Test", WIDTH, HEIGHT);
    let mut event_pump = sdl_context.event_pump()?;

    let border = Polygon {
        points: vec![
            Vec2 { x: 0.0, y: 0.0 },
            Vec2 {
                x: 0.0,
                y: HEIGHT as f32,
            },
            Vec2 {
                x: WIDTH as f32,
                y: HEIGHT as f32,
            },
            Vec2 {
                x: WIDTH as f32,
                y: 0.0,
            },
        ],
    };

    let mut player = Player {
        transform: Transform {
            scale: Vec2 { x: 1.0, y: 1.0 },
            translate: Vec2 { x: 100.0, y: 100.0 },
            rotate: 0.0,
        },
        points: Polygon {
            points: vec![
                Vec2 { x: 30.0, y: 40.0 },
                Vec2 { x: -30.0, y: 40.0 },
                Vec2 { x: 0.0, y: -60.0 },
            ],
        },
        color: sdl2::pixels::Color::RGB(0, 155, 0),
        speed: 5.0,
    };

    let object = Object {
        transform: Transform {
            scale: Vec2 { x: 1.0, y: 1.0 },
            translate: Vec2 { x: 500.0, y: 500.0 },
            rotate: 0.0,
        },
        points: vec![
            Vec2 { x: -100.0, y: 0.0 },
            Vec2 { x: 0.0, y: 100.0 },
            Vec2 { x: 100.0, y: 100.0 },
            Vec2 { x: 100.0, y: 0.0 },
            Vec2 { x: 0.0, y: -100.0 },
        ],
        color: sdl2::pixels::Color::RGB(0, 0, 155),
    };

    let mut draw_bounding_box = false;
    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,
                Event::KeyDown {
                    keycode: Some(Keycode::B),
                    ..
                } => draw_bounding_box = !draw_bounding_box,
                _ => {}
            }
        }

        player.frame_update(Some(&event_pump));
        renderer.draw_color(player.color);
        renderer.draw_polygon(&player.polygon())?;
        if draw_bounding_box {
            renderer.draw_polygon(&gen_bounding_box(&player))?;
        }

        renderer.draw_polygon(&border)?;

        if aabb_collision(&player, &object) {
            renderer.draw_color(sdl2::pixels::Color::RGB(155, 0, 0));
            let trans = epa(&player, &object);
            player.transform.translate.x -= trans.x;
            player.transform.translate.y -= trans.y;
        } else {
            renderer.draw_color(object.color);
        }

        renderer.draw_polygon(&object.polygon())?;
        if draw_bounding_box {
            renderer.draw_polygon(&gen_bounding_box(&object))?;
        }

        renderer.draw_color(sdl2::pixels::Color::RGB(0, 0, 0));
        renderer.display();
    }

    Ok(())
}
