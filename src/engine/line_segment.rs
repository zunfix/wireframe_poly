use float_cmp::approx_eq;

use super::float_range::Range;
use super::vec2::Vec2;

#[derive(Debug, Copy, Clone)]
pub struct LineSeg {
    pub p1: Vec2,
    pub p2: Vec2,
}


impl PartialEq for LineSeg {
    fn eq(&self, other: &Self) -> bool {
        (self.p1 == other.p1) && (self.p2 == other.p2)
    }
}


impl LineSeg {
    pub fn normal_to_origin(&self) -> Vec2 {
        let c = self.p1.translate(&self.p2.negate());
        // Only care about magnitude, or else direction vector will be way of in space
        let z = c.cross_scaler(&self.p2).signum();
        Vec2 {
            x: c.y * z,
            y: -1.0 * (c.x * z),
        }
    }


    pub fn closest_point_to_org(&self) -> Vec2 {
        let c = self.p2.translate(&self.p1.negate());
        let len_sq = c.len_sqrd();
        let param = if len_sq != 0.0 {
            c.dot(&self.p1.negate()) / len_sq
        } else {
            -1.0
        };

        if param < 0.0 {
            self.p1
        } else if param > 1.0 {
            self.p2
        } else {
            Vec2 {
                x: self.p1.x + param * c.x,
                y: self.p1.y + param * c.y,
            }
        }
    }
}

// Returns the point that a horizontal line, b, intersects a line segment. If there is no
// intersection None is returned.
pub fn segment_horizontal_line_collision(seg: &LineSeg, b: &f32) -> Option<Vec2> {
    if b.in_range_inc(&seg.p1.y, &seg.p2.y) {
        // vertical line
        if approx_eq!(f32, seg.p1.x, seg.p2.x, ulps = 2) {
            Some(Vec2 { x: seg.p1.x, y: *b })
        } else {
            let x_inter =
                seg.p1.x + (((b - seg.p1.y) * (seg.p2.x - seg.p1.x)) / (seg.p2.y - seg.p1.y));
            Some(Vec2 { x: x_inter, y: *b })
        }
    } else {
        None
    }
}


#[cfg(test)]
mod line_and_segment_collision {
    use super::super::vec2::I_HAT;
    use super::*;

    #[test]
    fn line_and_segment_that_intersect_returns_a_point() {
        let s = LineSeg {
            p1: Vec2 { x: 0.0, y: 0.0 },
            p2: Vec2 { x: 3.0, y: 3.0 },
        };
        assert_eq!(
            segment_horizontal_line_collision(&s, &2.0).unwrap(),
            Vec2 { x: 2.0, y: 2.0 }
        );
    }


    #[test]
    fn parallel_line_and_segment_intersect_at_none() {
        let s = LineSeg {
            p1: Vec2 { x: 3.0, y: 3.0 },
            p2: Vec2 { x: 0.0, y: 3.0 },
        };
        assert!(segment_horizontal_line_collision(&s, &2.0).is_none())
    }


    #[test]
    fn vertical_line_segment_intersection() {
        let s = LineSeg {
            p1: Vec2 { x: 3.0, y: 3.0 },
            p2: Vec2 { x: 3.0, y: 0.0 },
        };
        assert_eq!(
            segment_horizontal_line_collision(&s, &2.0).unwrap(),
            Vec2 { x: 3.0, y: 2.0 }
        );
    }


    #[test]
    fn closest_point() {
        let s = LineSeg {
            // p1: Vec2 { x: 1.0, y: 175.0 },
            // p2: Vec2 { x: 1.0, y: -25.0 },
            p1: Vec2 { x: 1.0, y: 2.0 },
            p2: Vec2 { x: 1.0, y: -1.0 },
        };
        assert_eq!(s.closest_point_to_org(), I_HAT);
    }
}
