use float_cmp::approx_eq;

#[derive(Debug, Copy, Clone)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}


pub const I_HAT: Vec2 = Vec2 { x: 1.0, y: 0.0 };
pub const J_HAT: Vec2 = Vec2 { x: 0.0, y: 1.0 };


impl PartialEq for Vec2 {
    fn eq(&self, other: &Self) -> bool {
        // Minimum Epsilon to allow unit tests to pass
        let ep = 0.0000002;
        approx_eq!(f32, self.x, other.x, epsilon = ep)
            && approx_eq!(f32, self.y, other.y, epsilon = ep)
    }
}


impl Vec2 {
    pub fn dot(&self, other: &Vec2) -> f32 {
        (self.x * other.x) + (self.y * other.y)
    }


    pub fn scale(&self, other: &Vec2) -> Vec2 {
        Vec2 {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }


    pub fn translate(&self, other: &Vec2) -> Vec2 {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }


    pub fn rotate(&self, radians: &f32) -> Vec2 {
        Vec2 {
            x: (self.x * radians.cos()) - (self.y * radians.sin()),
            y: (self.y * radians.cos()) + (self.x * radians.sin()),
        }
    }


    pub fn cross_scaler(&self, vec: &Vec2) -> f32 {
        (self.x * vec.y) - (self.y * vec.x)
    }


    pub fn negate(&self) -> Vec2 {
        Vec2 {
            x: -1.0 * self.x,
            y: -1.0 * self.y,
        }
    }


    pub fn len_sqrd(&self) -> f32 {
        (self.x * self.x) + (self.y * self.y)
    }
}

#[cfg(test)]
mod vec2_test {
    use super::*;
    #[test]
    fn len_test() {
        assert_eq!(I_HAT.len_sqrd(), 1.0);
        assert_eq!((Vec2 { x: -3.0, y: -4.0 }).len_sqrd(), 25.0);
        assert_eq!((Vec2 { x: 3.0, y: -4.0 }).len_sqrd(), 25.0);
        assert_eq!((Vec2 { x: -3.0, y: 4.0 }).len_sqrd(), 25.0);
    }


    mod dot_product {
        use super::super::*;
        #[test]
        fn vec2_normal_to_each_other_have_dot_of_zero() {
            let v1 = Vec2 { x: 1.0, y: 0.0 };
            let v2 = Vec2 { x: 0.0, y: 5.0 };
            assert!(approx_eq!(f32, v1.dot(&v2), 0.0, ulps = 2));
            assert!(approx_eq!(f32, v2.dot(&v1), 0.0, ulps = 2));
        }


        #[test]
        fn vec2s_pi_radians_apart_have_dot_of_neg_one() {
            let v1 = Vec2 { x: 1.0, y: 0.0 };
            let v2 = Vec2 { x: -1.0, y: 0.0 };
            assert!(approx_eq!(f32, v1.dot(&v2), -1.0, ulps = 2));
            assert!(approx_eq!(f32, v2.dot(&v1), -1.0, ulps = 2));
        }


        #[test]
        fn identical_vec2s_have_dot_of_one() {
            let v1 = Vec2 { x: 1.0, y: 0.0 };
            assert!(approx_eq!(f32, v1.dot(&v1), 1.0, ulps = 2));
        }
    }


    mod scale {
        use super::super::*;

        #[test]
        fn vec2_scaled_by_unit_vec2_returns_self() {
            let v1 = Vec2 { x: 8.0, y: 3.0 };
            let unit = Vec2 { x: 1.0, y: 1.0 };
            assert_eq!(v1.scale(&unit), v1);
        }


        #[test]
        fn vec2_scaled_by_unit_x_vec2_returns_x_proj() {
            let v1 = Vec2 { x: 8.0, y: 3.0 };
            let x_proj = Vec2 { x: 8.0, y: 0.0 };
            let unit_x = Vec2 { x: 1.0, y: 0.0 };
            assert_eq!(v1.scale(&unit_x), x_proj);
        }


        #[test]
        fn vec2_scaled_by_zero_returns_zero() {
            let v1 = Vec2 { x: 8.0, y: 3.0 };
            let zero = Vec2 { x: 0.0, y: 0.0 };
            assert_eq!(v1.scale(&zero), zero);
        }
    }


    mod translate {
        use super::super::*;

        #[test]
        fn vec2_translated_by_unit_vec2_increases_x_and_y_by_1() {
            let v1 = Vec2 { x: 8.0, y: 3.0 };
            let v1_inc = Vec2 { x: 9.0, y: 4.0 };
            let unit = Vec2 { x: 1.0, y: 1.0 };
            assert_eq!(v1.translate(&unit), v1_inc);
        }


        #[test]
        fn vec2_translated_by_zero_returns_self() {
            let v1 = Vec2 { x: 8.0, y: 3.0 };
            let zero = Vec2 { x: 0.0, y: 0.0 };
            assert_eq!(v1.translate(&zero), v1);
        }
    }


    mod rotate {
        use super::super::*;
        use std::f32::consts::PI;

        #[test]
        fn unit_x_rotated_by_pi_over_2_rad_returns_unit_y() {
            let unit_x = Vec2 { x: 1.0, y: 0.0 };
            let unit_y = Vec2 { x: 0.0, y: 1.0 };
            assert_eq!(unit_x.rotate(&(PI / 2.0)), unit_y);
        }


        #[test]
        fn vec2_rotated_by_zero_returns_self() {
            let v1 = Vec2 { x: 8.0, y: 3.0 };
            assert_eq!(v1.rotate(&0.0), v1);
        }
    }
}
