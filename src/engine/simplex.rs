use super::line_segment::LineSeg;
use super::polygon::Polygon;
use super::vec2::{Vec2, I_HAT};

// Stores the points of the simplex. A is the newest added point, C is the oldest
pub struct Simplex2 {
    pub a: Option<Vec2>,
    pub b: Option<Vec2>,
    pub c: Option<Vec2>,
}


enum SimplexRemove {
    B,
    C,
}


enum SimplexSide {
    AB,
    AC,
    BC,
}


impl Simplex2 {
    pub fn new() -> Simplex2 {
        Simplex2 {
            a: None,
            b: None,
            c: None,
        }
    }


    pub fn add_point(&mut self, new_point: &Vec2) {
        match self.a {
            Some(_p) => {
                self.remove(SimplexRemove::C);
                self.a = Some(*new_point);
            }
            None => match self.c {
                None => self.c = Some(*new_point),
                Some(_p) => match self.b {
                    None => self.b = Some(*new_point),
                    _ => self.a = Some(*new_point),
                },
            },
        }
    }


    pub fn update_simplex(&mut self) -> Option<Vec2> {
        match self.num_points() {
            3 => self.triangle_case(),
            2 => Some(self.side(SimplexSide::BC).normal_to_origin()),
            1 => Some(self.c.unwrap().negate()),
            _ => Some(I_HAT),
        }
    }


    pub fn to_polygon(&self) -> Polygon {
        Polygon {
            points: vec![self.a.unwrap(), self.b.unwrap(), self.c.unwrap()],
        }
    }

    
    fn side(&self, side: SimplexSide) -> LineSeg {
        match side {
            SimplexSide::AB => LineSeg {
                p1: self.a.unwrap(),
                p2: self.b.unwrap(),
            },
            SimplexSide::AC => LineSeg {
                p1: self.a.unwrap(),
                p2: self.c.unwrap(),
            },
            SimplexSide::BC => LineSeg {
                p1: self.b.unwrap(),
                p2: self.c.unwrap(),
            },
        }
    }

   
    fn remove(&mut self, select: SimplexRemove) {
        if let SimplexRemove::C = select {
            self.c = self.b
        }

        self.b = self.a;
        self.a = None;
    }

   
    fn num_points(&self) -> u8 {
        if self.a.is_some() {
            3
        } else if self.b.is_some() {
            2
        } else if self.c.is_some() {
            1
        } else {
            0
        }
    }


    fn triangle_case(&mut self) -> Option<Vec2> {
        if self
            .side(SimplexSide::AB)
            .normal_to_origin()
            .dot(&self.c.unwrap())
            < 0.0
        {
            // Origin in AB region
            let dir = Some(self.side(SimplexSide::AB).normal_to_origin());
            self.remove(SimplexRemove::C);
            dir
        } else if self
            .side(SimplexSide::AC)
            .normal_to_origin()
            .dot(&self.b.unwrap())
            < 0.0
        {
            // Origin in AC region
            let dir = Some(self.side(SimplexSide::AC).normal_to_origin());
            self.remove(SimplexRemove::B);
            dir
        } else {
            // Origin in Simplex
            None
        }
    }
}

#[cfg(test)]
mod simplex2_test {
    use super::*;

    #[test]
    fn simplex_with_no_points_adds_to_c() {
        let mut sim = Simplex2 {
            a: None,
            b: None,
            c: None,
        };
        assert_eq!(sim.num_points(), 0);
        sim.add_point(&Vec2 { x: 1.0, y: 1.0 });
        assert_eq!(sim.c.unwrap(), Vec2 { x: 1.0, y: 1.0 });
        assert_eq!(sim.num_points(), 1);
    }


    #[test]
    fn simplex_with_one_point_adds_to_b() {
        let mut sim = Simplex2 {
            a: None,
            b: None,
            c: Some(Vec2 { x: 0.5, y: 0.5 }),
        };
        sim.add_point(&Vec2 { x: 1.0, y: 1.0 });
        assert_eq!(sim.b.unwrap(), Vec2 { x: 1.0, y: 1.0 });
        assert_eq!(sim.num_points(), 2);
    }


    #[test]
    fn simplex_with_two_points_adds_to_a() {
        let mut sim = Simplex2 {
            a: None,
            b: Some(Vec2 { x: -0.5, y: 0.5 }),
            c: Some(Vec2 { x: 0.5, y: 0.5 }),
        };
        sim.add_point(&Vec2 { x: 1.0, y: 1.0 });
        assert_eq!(sim.a.unwrap(), Vec2 { x: 1.0, y: 1.0 });
        assert_eq!(sim.num_points(), 3);
    }


    #[test]
    fn simplex_with_three_points_removes_c() {
        let mut sim = Simplex2 {
            a: Some(Vec2 { x: 0.5, y: 0.5 }),
            b: Some(Vec2 { x: -0.5, y: 0.5 }),
            c: Some(Vec2 { x: -0.5, y: -0.5 }),
        };
        sim.add_point(&Vec2 { x: 1.0, y: 1.0 });
        assert_eq!(sim.a.unwrap(), Vec2 { x: 1.0, y: 1.0 });
        assert_eq!(sim.b.unwrap(), Vec2 { x: 0.5, y: 0.5 });
        assert_eq!(sim.c.unwrap(), Vec2 { x: -0.5, y: 0.5 });
    }


    #[test]
    fn remove_b() {
        let mut sim = Simplex2 {
            a: Some(Vec2 { x: 0.5, y: 0.5 }),
            b: Some(Vec2 { x: -0.5, y: 0.5 }),
            c: Some(Vec2 { x: -0.5, y: -0.5 }),
        };
        sim.remove(SimplexRemove::B);
        assert_eq!(sim.a, None);
        assert_eq!(sim.b.unwrap(), Vec2 { x: 0.5, y: 0.5 });
        assert_eq!(sim.c.unwrap(), Vec2 { x: -0.5, y: -0.5 });
    }


    #[test]
    fn remove_c() {
        let mut sim = Simplex2 {
            a: Some(Vec2 { x: 0.5, y: 0.5 }),
            b: Some(Vec2 { x: -0.5, y: 0.5 }),
            c: Some(Vec2 { x: -0.5, y: -0.5 }),
        };
        sim.remove(SimplexRemove::C);
        assert_eq!(sim.a, None);
        assert_eq!(sim.b.unwrap(), Vec2 { x: 0.5, y: 0.5 });
        assert_eq!(sim.c.unwrap(), Vec2 { x: -0.5, y: 0.5 });
    }
}
