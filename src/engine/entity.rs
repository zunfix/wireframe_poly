use super::float_range::Range;
use super::line_segment::{segment_horizontal_line_collision, LineSeg};
use super::polygon::Polygon;
use super::simplex::Simplex2;
use super::vec2::{Vec2, I_HAT};


pub trait Entity {
    /// Actions to take when a frame is updated.
    /// If the Entity does not update on a frame the body should be the following.
    /// ```
    /// if let Some(_event) = event_pump {}
    /// ```
    fn frame_update(&mut self, event_pump: Option<&sdl2::EventPump>);

    /// Returns the a polygon representing the Entity transformed into global space.
    fn polygon(&self) -> Polygon;

    /// Finds the farthest point in the entity
    fn farthest_point(&self, direction: &Vec2) -> Vec2;
}


/// Performs an Axis Aligned Bound Box Collision on two entities.
pub fn aabb_collision(ent1: &impl Entity, ent2: &impl Entity) -> bool {
    let shad1 = ent1.polygon().x_shadow();
    let shad2 = ent2.polygon().x_shadow();
    let x_collision = shad1.p2.x.in_range_inc(&shad2.p1.x, &shad2.p2.x)
        || shad2.p2.x.in_range_inc(&shad1.p1.x, &shad1.p2.x);

    let shad1 = ent1.polygon().y_shadow();
    let shad2 = ent2.polygon().y_shadow();
    let y_collision = shad1.p2.y.in_range_inc(&shad2.p1.y, &shad2.p2.y)
        || shad2.p2.y.in_range_inc(&shad1.p1.y, &shad1.p2.y);

    x_collision && y_collision
}


pub fn epa(ent1: &impl Entity, ent2: &impl Entity) -> Vec2 {
    let mut simplex = Simplex2::new();
    let mut support_point = support(ent1, ent2, &I_HAT);
    simplex.add_point(&support_point);
    // search in opposite direction for greatest chance of finding point on the
    // other side of the origin
    support_point = support_point.negate();

    'iterations: loop {
        let new_support = support(ent1, ent2, &support_point);
        if new_support.dot(&support_point) < 0.0 {
            // Farthest point in search direction does not cross origin, thus no
            // intersection
            return Vec2 { x: 0.0, y: 0.0 };
        }

        simplex.add_point(&new_support);
        match simplex.update_simplex() {
            Some(p) => support_point = p,
            None => break 'iterations,
        }
    }

    expand_simplex(ent1, ent2, &mut simplex.to_polygon())
}


fn support(ent1: &impl Entity, ent2: &impl Entity, dir: &Vec2) -> Vec2 {
    let p1 = ent1.farthest_point(dir);
    let p2 = ent2.farthest_point(&dir.negate());
    Vec2 {
        x: p1.x - p2.x,
        y: p1.y - p2.y,
    }
}


fn expand_simplex(ent1: &impl Entity, ent2: &impl Entity, polytope: &mut Polygon) -> Vec2 {
    loop {
        let closest_edge = polytope.closest_edge_to_org();
        let point = support(ent1, ent2, &closest_edge.normal_to_origin().negate());

        match polytope.points.iter().find(|p| **p == point) {
            Some(_p) => return closest_edge.closest_point_to_org(),
            None => polytope.insert_point(&closest_edge, &point),
        }
    }
}


/// Test if a point is in an Entity in global space.
pub fn point_in_entity(point: &Vec2, ent: &impl Entity) -> bool {
    let points = ent.polygon().points;
    let mut next_point = points.iter();
    let first_point = next_point.next().unwrap();

    let mut inter_to_left = 0;
    for start_point in points.iter().take_while(|p| *p != points.last().unwrap()) {
        inter_to_left += point_on_left(
            LineSeg {
                p1: *start_point,
                p2: *next_point.next().unwrap(),
            },
            point,
        );
    }

    inter_to_left += point_on_left(
        LineSeg {
            p1: *first_point,
            p2: *points.last().unwrap(),
        },
        point,
    );
    inter_to_left % 2 != 0
}


fn point_on_left(seg: LineSeg, point: &Vec2) -> i32 {
    let intersect = segment_horizontal_line_collision(&seg, &point.y);

    match intersect {
        Some(inter) => {
            if inter.x < point.x {
                1
            } else {
                0
            }
        }
        None => 0,
    }
}


#[cfg(test)]
mod collision_test {
    #[cfg(test)]
    mod bounding_box_collisions {
        use super::super::super::vec2::Vec2;
        use super::super::*;

        struct Poly {
            polygon: Polygon,
        }

        impl Entity for Poly {
            fn polygon(&self) -> Polygon {
                let points = self.polygon.points.to_vec();
                Polygon { points }
            }

            fn frame_update(&mut self, event_pump: Option<&sdl2::EventPump>) {
                match event_pump {
                    Some(_event) => {}
                    None => {}
                }
            }

            fn farthest_point(&self, direction: &Vec2) -> Vec2 {
                self.polygon.farthest_point(direction)
            }
        }


        #[test]
        fn intersecting_polygons_intersect() {
            let poly1 = Poly {
                polygon: Polygon {
                    points: vec![
                        Vec2 { x: 0.0, y: 0.9 },
                        Vec2 { x: 1.1, y: 1.0 },
                        Vec2 { x: 1.1, y: 0.9 },
                    ],
                },
            };

            let poly2 = Poly {
                polygon: Polygon {
                    points: vec![
                        Vec2 { x: 0.1, y: 0.0 },
                        Vec2 { x: 1.0, y: 1.1 },
                        Vec2 { x: 1.0, y: 0.0 },
                    ],
                },
            };

            assert!(gjk(&poly1, &poly2));
        }


        #[test]
        fn non_intersecting_polygons_dont_intersect() {
            let poly1 = Poly {
                polygon: Polygon {
                    points: vec![
                        Vec2 { x: 10.0, y: 0.9 },
                        Vec2 { x: 11.1, y: 1.0 },
                        Vec2 { x: 11.1, y: 0.9 },
                    ],
                },
            };

            let poly2 = Poly {
                polygon: Polygon {
                    points: vec![
                        Vec2 { x: 0.1, y: 0.0 },
                        Vec2 { x: 1.0, y: 1.1 },
                        Vec2 { x: 1.0, y: 0.0 },
                    ],
                },
            };

            assert!(!gjk(&poly1, &poly2));
        }
    }


    #[cfg(test)]
    mod point_in_entity {
        use super::super::super::vec2::Vec2;
        use super::super::*;

        struct Object {
            pub center: Vec2,
            pub points: Vec<Vec2>,
        }

        impl Entity for Object {
            fn polygon(&self) -> Polygon {
                let mut poly = vec![];
                for point in self.points.iter() {
                    poly.push(Vec2 {
                        x: (point.x + self.center.x),
                        y: (point.y + self.center.y),
                    })
                }
                Polygon { points: poly }
            }

            fn frame_update(&mut self, event_pump: Option<&sdl2::EventPump>) {
                match event_pump {
                    Some(_event) => {}
                    None => {}
                }
            }

            fn farthest_point(&self, _direction: &Vec2) -> Vec2 {
                Vec2 { x: 0.0, y: 0.0 }
            }
        }


        #[test]
        fn point_in_object_is_in_object() {
            let object = Object {
                center: Vec2 { x: 0.0, y: 0.0 },
                points: vec![
                    Vec2 { x: -5.0, y: -5.0 },
                    Vec2 { x: 5.0, y: -5.0 },
                    Vec2 { x: 5.0, y: 5.0 },
                    Vec2 { x: -5.0, y: 5.0 },
                ],
            };
            let point = Vec2 { x: 1.0, y: 1.0 };
            assert!(point_in_entity(&point, &object));
        }


        #[test]
        fn point_not_in_object_is_not_in_object() {
            let object = Object {
                center: Vec2 { x: 0.0, y: 0.0 },
                points: vec![
                    Vec2 { x: -5.0, y: -5.0 },
                    Vec2 { x: 5.0, y: -5.0 },
                    Vec2 { x: 5.0, y: 5.0 },
                    Vec2 { x: -5.0, y: 5.0 },
                ],
            };
            let point = Vec2 { x: 0.0, y: -10.0 };
            assert!(!point_in_entity(&point, &object));
        }
    }
}
