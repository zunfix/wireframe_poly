pub trait Range {
    fn in_range_inc(&self, a: &f32, b: &f32) -> bool;
}


impl Range for f32 {
    // Check if a float is between two other floats inclusively.
    // Order of a and b does not matter.
    //
    // # Examples
    // ```
    // let number: f32 = 5.0;
    // assert!(number.in_range_inc(&5.5, &6.0));
    // assert!(number.in_range_inc(&6.0, &5.0));
    // ```
    fn in_range_inc(&self, a: &f32, b: &f32) -> bool {
        (a.min(*b)..a.max(*b)).contains(&self)
    }
}


#[cfg(test)]
mod float_range {
    use super::*;

    #[test]
    fn three_is_in_range_of_one_and_four() {
        let num = 2.0;
        assert!(num.in_range_inc(&1.0, &4.0));
        assert!(num.in_range_inc(&4.0, &1.0));
    }


    #[test]
    fn three_is_in_range_inc_of_three_and_four() {
        let num = 3.0;
        assert!(num.in_range_inc(&4.0, &3.0));
        assert!(num.in_range_inc(&3.0, &4.0));
    }


    #[test]
    fn neg_three_is_not_in_range_of_three_and_four() {
        let num = -3.0;
        assert!(!num.in_range_inc(&4.0, &3.0));
        assert!(!num.in_range_inc(&3.0, &4.0));
    }
}
