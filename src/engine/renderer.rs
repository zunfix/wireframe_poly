use std::time::Duration;

use sdl2::rect;
use sdl2::render;
use sdl2::video;

use super::line_segment::LineSeg;
use super::polygon::Polygon;

pub struct Renderer {
    canvas: render::Canvas<video::Window>,
}

impl Renderer {
    pub fn new(sdl_context: &sdl2::Sdl, title: &str, width: u32, height: u32) -> Renderer {
        let video_subsystem = match sdl_context.video() {
            Ok(video_sys) => video_sys,
            Err(error) => panic!("Failed to init SDL Video Subsystem with error: {:?}", error),
        };

        let window = match video_subsystem
            .window(title, width, height)
            .position_centered()
            .build()
            .map_err(|e| e.to_string())
        {
            Ok(win) => win,
            Err(error) => panic!("Failed to init SDL Window with error: {:?}", error),
        };

        let canvas = match window
            .into_canvas()
            .accelerated()
            .build()
            .map_err(|e| e.to_string())
        {
            Ok(can) => can,
            Err(error) => panic!("Failed to init SDL Canvas with error: {:?}", error),
        };

        Renderer { canvas }
    }

    pub fn draw_color(&mut self, color: sdl2::pixels::Color) {
        self.canvas.set_draw_color(color);
    }

    pub fn display(&mut self) {
        self.canvas.present();
        std::thread::sleep(Duration::from_millis(10));
        self.canvas.clear();
    }

    pub fn draw_line(&mut self, seg: &LineSeg) -> Result<(), String> {
        let mut x1 = seg.p1.x.round() as i32;
        let mut y1 = seg.p1.y.round() as i32;
        let mut x2 = seg.p2.x.round() as i32;
        let mut y2 = seg.p2.y.round() as i32;

        let is_steep = (y2 - y1).abs() > (x2 - x1).abs();
        if is_steep {
            std::mem::swap(&mut x1, &mut y1);
            std::mem::swap(&mut x2, &mut y2);
        }

        if x1 > x2 {
            std::mem::swap(&mut x1, &mut x2);
            std::mem::swap(&mut y1, &mut y2);
        }

        let dx = x2 - x1;
        let dy = (y2 - y1).abs();
        let mut err = dx / 2;
        let mut y = y1;
        let ystep = if y1 < y2 { 1 } else { -1 };

        for x in x1..(x2 + 1) {
            if is_steep {
                self.canvas.draw_point(rect::Point::new(y, x))?;
            } else {
                self.canvas.draw_point(rect::Point::new(x, y))?;
            }
            err -= dy;
            if err < 0 {
                y += ystep;
                err += dx;
            }
        }

        Ok(())
    }


    pub fn draw_polygon(&mut self, poly: &Polygon) -> Result<(), String> {
        for seg in poly.line_segments().iter() {
            self.draw_line(seg)?;
        }

        Ok(())
    }
}
