use super::line_segment::LineSeg;
use super::vec2::Vec2;

pub struct Polygon {
    // Contains the vertices of the polygon in the order that they will be drawn.
    // The last point in the vector will be connected to the first point when the polygon is
    // drawn.
    pub points: Vec<Vec2>,
}


pub struct Transform {
    pub scale: Vec2,
    pub rotate: f32,
    pub translate: Vec2,
}


impl Polygon {
    pub fn transform(&self, trans: &Transform) -> Polygon {
        let points: Vec<Vec2> = self.points.iter().map(|p| p.scale(&trans.scale)).collect();
        let points: Vec<Vec2> = points.iter().map(|p| p.rotate(&trans.rotate)).collect();
        Polygon {
            points: points
                .iter()
                .map(|p| p.translate(&trans.translate))
                .collect(),
        }
    }


    // Projects the polygon onto the x-axis with p1.x < p2.x.
    //
    // ```
    // let poly = Polygon { points: vec![Vec2 {x: 1.0, y: 1.0},Vec2 {x: 0.0, y: 0.0}] }
    // assert_eq!(poly.x_shadow(), LineSeg{ p1: Vec2{ x: 0.0, y: 0.0 }, p2: Vec2{ x: 1.0, y: 0.0 } })
    // ```
    pub fn x_shadow(&self) -> LineSeg {
        let min = self
            .points
            .iter()
            .min_by(|p1, p2| p1.x.partial_cmp(&p2.x).unwrap())
            .unwrap();
        let max = self
            .points
            .iter()
            .max_by(|p1, p2| p1.x.partial_cmp(&p2.x).unwrap())
            .unwrap();
        LineSeg {
            p1: Vec2 { x: min.x, y: 0.0 },
            p2: Vec2 { x: max.x, y: 0.0 },
        }
    }


    // Projects the polygon onto the y-axis with p1.y < p2.y.
    //
    // ```
    // let poly = Polygon { points: vec![Vec2 {x: 1.0, y: 1.0}, Vec2 {x: 0.0, y: 0.0}] }
    // assert_eq!(poly.x_shadow(), LineSeg{ p1: Vec2{ x: 0.0, y: 0.0 }, p2: Vec2{ x: 0.0, y: 1.0 } })
    // ```
    pub fn y_shadow(&self) -> LineSeg {
        let min = self
            .points
            .iter()
            .min_by(|p1, p2| p1.y.partial_cmp(&p2.y).unwrap())
            .unwrap();
        let max = self
            .points
            .iter()
            .max_by(|p1, p2| p1.y.partial_cmp(&p2.y).unwrap())
            .unwrap();
        LineSeg {
            p1: Vec2 { x: 0.0, y: min.y },
            p2: Vec2 { x: 0.0, y: max.y },
        }
    }


    pub fn farthest_point(&self, direction: &Vec2) -> Vec2 {
        *self
            .points
            .iter()
            .max_by(|a, b| a.dot(direction).partial_cmp(&b.dot(direction)).unwrap())
            .unwrap()
    }


    pub fn line_segments(&self) -> Vec<LineSeg> {
        let mut segs = Vec::new();
        let mut next_point = self.points.iter();
        let first_point = next_point.next().unwrap();

        for point in self
            .points
            .iter()
            .take_while(|p| *p != self.points.last().unwrap())
        {
            segs.push(LineSeg {
                p1: *point,
                p2: *next_point.next().unwrap(),
            });
        }

        // close the polygon
        segs.push(LineSeg {
            p1: *self.points.iter().last().unwrap(),
            p2: *first_point,
        });

        segs
    }

    
    pub fn insert_point(&mut self, seg: &LineSeg, p: &Vec2) {
        let mut new_points = Vec::new();
        let mut inserted = false;

        for point in self.points.iter() {
            new_points.push(*point);
            if !inserted
                && ((*new_points.iter().last().unwrap() == seg.p1)
                    || (*new_points.iter().last().unwrap() == seg.p2))
            {
                inserted = true;
                new_points.push(*p);
            }
        }
        self.points = new_points;
    }


    pub fn closest_edge_to_org(&self) -> LineSeg {
        *self
            .line_segments()
            .iter()
            .min_by(|a, b| {
                a.closest_point_to_org()
                    .len_sqrd()
                    .partial_cmp(&b.closest_point_to_org().len_sqrd())
                    .unwrap()
            })
            .unwrap()
    }
}

#[cfg(test)]
mod polygon_test {
    use super::*;

    #[test]
    fn x_shadow() {
        let polygon = Polygon {
            points: vec![
                Vec2 { x: 0.0, y: 0.0 },
                Vec2 { x: 4.0, y: 0.0 },
                Vec2 { x: 7.0, y: 5.0 },
            ],
        };

        assert_eq!(
            polygon.x_shadow(),
            LineSeg {
                p1: Vec2 { x: 0.0, y: 0.0 },
                p2: Vec2 { x: 7.0, y: 0.0 }
            }
        );
    }


    #[test]
    fn y_shadow() {
        let polygon = Polygon {
            points: vec![
                Vec2 { x: 0.0, y: 0.0 },
                Vec2 { x: 4.0, y: 0.0 },
                Vec2 { x: 7.0, y: 5.0 },
            ],
        };

        assert_eq!(
            polygon.y_shadow(),
            LineSeg {
                p1: Vec2 { x: 0.0, y: 0.0 },
                p2: Vec2 { x: 0.0, y: 5.0 }
            }
        );
    }


    #[test]
    fn farthest_point() {
        let polygon = Polygon {
            points: vec![
                Vec2 { x: 1.0, y: 1.0 },
                Vec2 { x: 1.0, y: -1.0 },
                Vec2 { x: -1.0, y: -1.0 },
                Vec2 { x: -1.0, y: 1.0 },
            ],
        };

        let mut search_direction = Vec2 { x: 1.0, y: 1.0 };
        assert_eq!(
            polygon.farthest_point(&search_direction),
            Vec2 { x: 1.0, y: 1.0 }
        );

        search_direction = Vec2 { x: -1.0, y: 0.1 };
        assert_eq!(
            polygon.farthest_point(&search_direction),
            Vec2 { x: -1.0, y: 1.0 }
        );
    }


    #[test]
    fn insert_point() {
        let mut polygon = Polygon {
            points: vec![
                Vec2 { x: 1.0, y: 1.0 },
                // Vec2 { x: 1.0, y: -1.0 },
                Vec2 { x: -1.0, y: -1.0 },
                Vec2 { x: -1.0, y: 1.0 },
            ],
        };

        polygon.insert_point(
            &LineSeg {
                p1: polygon.points[0],
                p2: polygon.points[1],
            },
            &Vec2 { x: 1.0, y: -1.0 },
        );
        assert_eq!(polygon.points[0], Vec2 { x: 1.0, y: 1.0 });
        assert_eq!(polygon.points[1], Vec2 { x: 1.0, y: -1.0 });
        assert_eq!(polygon.points[2], Vec2 { x: -1.0, y: -1.0 });
    }


    #[test]
    fn closest_edge() {
        let mut polygon = Polygon {
            points: vec![
                Vec2 { x: 1.0, y: 1.0 },
                Vec2 { x: 1.0, y: -1.0 },
                Vec2 { x: -5.0, y: -5.0 },
                Vec2 { x: -5.0, y: 5.0 },
            ],
        };
        let closest_edge = LineSeg {
            p1: Vec2 { x: 1.0, y: 1.0 },
            p2: Vec2 { x: 1.0, y: -1.0 },
        };
        assert_eq!(polygon.closest_edge_to_org(), closest_edge);
    }
}
